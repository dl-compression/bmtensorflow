pkill -f jupyter-notebook
jupyter notebook "$@" --allow-root --port=8888 &
tensorboard --logdir=/log/tensorboard