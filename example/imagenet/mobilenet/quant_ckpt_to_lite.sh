#!/usr/bin/env bash
# Take mobilenet v1 as example
# Download the pretrained model from
# http://download.tensorflow.org/models/mobilenet_v1_2018_02_22/mobilenet_v1_1.0_224_quant.tgz
CKPT_PATH=./mobilenet_v1_1.0_224_quant

python /opt/tf_model/research/slim/export_inference_graph.py \
        --alsologtostderr \
        --model_name=mobilenet_v1 \
        --quantize=True \
        --output_file=./mobilenet_v1_inf_quant.pb

freeze_graph --input_graph=./mobilenet_v1_inf_quant.pb \
             --input_binary=true \
             --input_checkpoint=${CKPT_PATH}/mobilenet_v1_1.0_224_quant.ckpt \
             --output_node_names=MobilenetV1/Predictions/Reshape_1 \
             --output_graph=./mobilenet_v1_1.0_224_quant.pb

toco --graph_def_file=./mobilenet_v1_1.0_224_quant.pb \
     --output_file=./mobilenet_v1_1.0_224_quant.tflite \
     --output_format=TFLITE \
     --inference_type=QUANTIZED_UINT8 \
     --inference_input_type=QUANTIZED_UINT8 \
     --input_array=input \
     --output_array=MobilenetV1/Predictions/Reshape_1 \
     --mean_values=127.5 \
     --std_dev_values=127.5
