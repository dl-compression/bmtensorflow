#!/usr/bin/env bash
NUM_GPUS=1

# download pre train check point
# wget http://download.tensorflow.org/models/mobilenet_v1_2018_02_22/mobilenet_v1_1.0_224.tgz
# --learning_rate - slim internally averages clones so we compensate the learning_rate
# --num_epochs_per_decay - train_image_classifier does per clone epochs
cd ..
python train_image_classifier.py --model_name="mobilenet_v1" \
    --learning_rate=0.045 * ${NUM_GPUS} \
    --preprocessing_name="mobilenet_v1" \
    --label_smoothing=0.1 \
    --moving_average_decay=0.9999 \
    --batch_size=32 \
    --num_clones=${NUM_GPUS} \
    --learning_rate_decay_factor=0.98 \
    --num_epochs_per_decay=2.5 / ${NUM_GPUS} \
    --quantize_delay=1000 \
    --dataset_dir=/data/dataset/imagenet/tfrecord \
    --ignore_missing_vars=True \
    --checkpoint_path="/workspace/example/imagenet/mobilenet/mobilenet_v1_1.0_224/mobilenet_v1_1.0_224.ckpt" \
    --train_dir="/workspace/example/imagenet/mobilenet/mobilenet_train"