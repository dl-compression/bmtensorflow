from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import os
from PIL import Image
from tensorflow.contrib.lite.python import interpreter as interpreter_wrapper

label_file = '../test_data/labels.txt'
data_path = '/data/dataset/imagenet/raw'
# model_file = 'mobilenet_v1_1.0_224.tflite'
model_file = 'mobilenet_v1_1.0_224_quant.tflite'
input_mean = 127.5
input_std = 127.5
nh, nw = 224, 224


def crop_center(img, crop_w, crop_h):
    h, w, _ = img.shape
    offset_h = int((h - crop_h) / 2)
    offset_w = int((w - crop_w) / 2)
    return img[offset_h:h - offset_h, offset_w:w - offset_w]


def print_result(image_count, top1_correct_count, top5_correct_count):
    print('image_count', image_count)
    print('top1_correct_count', top1_correct_count)
    print('top5_correct_count', top5_correct_count)
    print('top1 precision', top1_correct_count / image_count)
    print('top5 precision', top5_correct_count / image_count)


if __name__ == "__main__":
    # labels = load_labels(label_file)
    floating_model = False

    interpreter = interpreter_wrapper.Interpreter(model_path=model_file)
    interpreter.allocate_tensors()

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # check the type of the input tensor
    if input_details[0]['dtype'] == np.float32:
        floating_model = True

    # NxHxWxC, H:1, W:2
    height = input_details[0]['shape'][1]
    width = input_details[0]['shape'][2]

    image_count = 0
    top1_correct_count = 0
    top5_correct_count = 0

    for i in range(1001):
        class_path = os.path.join(data_path, str(i))
        imgs_name = os.listdir(class_path)

        for img_name in imgs_name:
            image_path = os.path.join(class_path, img_name)
            img = Image.open(image_path)
            img = np.array(img)
            img = crop_center(img, nh, nw)
            # add N dim
            input_data = np.expand_dims(img, axis=0)

            if floating_model:
                input_data = (np.float32(input_data) - input_mean) / input_std

            # print(input_data.shape)
            interpreter.set_tensor(input_details[0]['index'], input_data)
            interpreter.invoke()
            output_data = interpreter.get_tensor(output_details[0]['index'])
            results = np.squeeze(output_data)
            top_5 = results.argsort()[-5:][::-1]

            if top_5[0] == i:
                top1_correct_count += 1

            if np.any(top_5[0:5] == i):
                top5_correct_count += 1

            image_count += 1

        if image_count > 0:
            print_result(image_count, top1_correct_count, top5_correct_count)

