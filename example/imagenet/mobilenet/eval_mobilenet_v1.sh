#!/usr/bin/env bash
#CHECKPOINT_FILE=/workspace/example/imagenet/mobilenet/mobilenet_v1_1.0_224_quant/mobilenet_v1_1.0_224_quant.ckpt
CHECKPOINT_FILE=/workspace/example/imagenet/mobilenet/mobilenet_train/model.ckpt-1041
EVAL_DIR=/workspace/example/imagenet/mobilenet/mobilenet_eval

cd ..
python eval_image_classifier.py \
    --alsologtostderr \
    --checkpoint_path=${CHECKPOINT_FILE} \
    --eval_dir=${EVAL_DIR} \
    --dataset_name=imagenet \
    --dataset_dir=/data/dataset/imagenet/tfrecord \
    --dataset_split_name=validation \
    --model_name=mobilenet_v1 \
    --preprocessing_name=mobilenet_v1 \
    --quantize=True
